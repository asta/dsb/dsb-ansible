function get(url) {
	// Return a new promise.
	return new Promise(function(resolve, reject) {
		// Do the usual XHR stuff
		var req = new XMLHttpRequest();
		req.open('GET', url);

		req.onload = function() {
			// This is called even on 404 etc
			// so check the status
			if (req.status === 200) {
				// Resolve the promise with the response text
				resolve(req.response);
			}
			else {
				// Otherwise reject with the status text
				// which will hopefully be a meaningful error
				reject(Error(req.statusText));
			}
		};

		// Handle network errors
		req.onerror = function() {
			reject(Error("Network Error"));
		};

		// Make the request
		req.send();
	});
}

function servertotableentry(s) {

	/*
	"asta-test01": {
		"ssh_ca_managed": true,
		"public_ip": "141.5.101.41",
		"public_domains": [
			"c101-041.cloud.gwdg.de",
			"test01.asta.uni-goettingen.de"
		],
		"asta_meta": {
			"account": "0asta",
			"description": [
				"für diverses Testen und Entwickeln von Software um Sachen nicht direkt auf Produktionsservern machen zu müssen",
				"kann jederzeit neu installiert werden (bitte zuständige Person vorher kontaktieren)"
			],
			"responsible": [
				"Jake"
			],
			"title": "test01"
		},
		"custom_principals": [
			"test_servers",
			"all"
		]
	},
	*/

	// Convert strings, that are supposed to be arrays, to arrays.
	if(typeof s.public_domains === 'string') {
		s.public_domains = [s.public_domains];
	}
	if(typeof s.custom_principals === 'string') {
		s.custom_principals = [s.custom_principals];
	}
	if(typeof s.asta_meta.description === 'string') {
		s.asta_meta.description = [s.asta_meta.description];
	}
	if(typeof s.asta_meta.responsible === 'string') {
		s.asta_meta.responsible = [s.asta_meta.responsible];
	}


	// add implicit principals
	s.custom_principals.push(s.serverName);
	s.custom_principals.push('admin');


	var res = "";
	res += `<tr id="${s.serverName}">`;

	// res += `<td><a href="#${s.serverName}">${s.asta_meta.title}</a></td>`;
	res += `<td><b>${s.asta_meta.title}</b></td>`;

	res += `<td><pre>${s.serverName}</pre></td>`;

	res += "<td>";
	if (s.asta_meta.description.length != 0) {
		res += "<ul>";
		res += s.asta_meta.description.map(p => `<li>${p}</li>`)?.join("");
		res += "</ul>";
	}
	res += "</td>";

	res += `<td><pre>${s.public_ip}</pre></td>`;

	res += "<td>";
	if (s.public_domains.length != 0) {
		res += "<ul>";
		res += s.public_domains.map(p => `<li><a href="http://${p}">${p}</a></li>`)?.join("");
		res += "</ul>";
	}
	res += "</td>";

	res += "<td>";
	if (s.asta_meta.responsible.length != 0) {
		res += "<ul>";
		res += s.asta_meta.responsible.map(p => `<li>${p}</li>`)?.join("");
		res += "</ul>";
	}
	res += "</td>";

	res += `<td>${s.asta_meta.account}</td>`;

	res += `<td class="bool bool-${s.ssh_ca_managed ? "true" : "false"}">${s.ssh_ca_managed ? "Ja" : "Nein"}</td>`;

	res += "<td><ul>";
	res += s.custom_principals.map(p => `<li>${p}</li>`)?.join("");
	res += "</ul></td>";


	res += "</tr>";
	return res;
}

async function loadData() {
	try{
		// const result = JSON.parse(await get("./ssh_ca_servers.json"));
		const result = JSON.parse(await get("https://asta.pages.gwdg.de/dsb/dsb-ansible/ssh_ca_servers.json"));
		console.log(result);
		const servers = result.servernames.map(s => {
			result.servers[s].serverName = s;
			return result.servers[s]
		});
		console.log(servers);
		const tableheader = `<tr>
				<th title="Servername/Titel">Titel</th>
				<th title="SSH Alias/Name">SSH</th>
				<th>Beschreibung</th>
				<th title="Öffentliche IP">IP</th>
				<th>Domains</th>
				<th title="Zuständige Personen">Zuständig</th>
				<th title="Funktionsaccount">Account</th>
				<th title="AStA SSH CA managed">CA</th>
				<th>Principals</th>
			</tr>`;
		/* const tablecontent = servers.map(s => 
			`<tr>
				<td>${s.serverName}</td>
				<td>${s.ssh_ca_managed ? "ja" : "nein"}</td>
				<td>${s.public_ip}</td>
				<td>${s.custom_principals?.join(", ")}</td>
				<td>${s.public_domains?.join(", ")}</td>
			</tr>`).join(""); */
		const tablecontent = servers.map(servertotableentry).join("");
		document.getElementById("content").innerHTML = `<table>${tableheader}${tablecontent}</table>`;
		console.log(`<table>${tableheader}${tablecontent}</table>`)
		
	} catch (e) {
		console.error(e);
		document.getElementById("content").innerHTML = `<div>${e}</div>`;

	}
}

window.onload = function() {
	loadData();
};

