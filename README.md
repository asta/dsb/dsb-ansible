# AStA Ansible

Das Ansible Inventory + allgemeine Playbooks für die Server des DSB des AStAs.

Siehe auch die [AStA Server Setup](https://gitlab.gwdg.de/asta/dnd-referat/ansible-role-asta_server_setup) Rolle.

[[_TOC_]]

## Abhängigkeiten

Du benötigst die folgende Software:

- [Ansible](https://www.ansible.com/) (Version: >=2.9.0)
- [Git](https://git-scm.com/)
- [Bash](https://www.gnu.org/software/bash/)
- [GNU Coreutils](https://www.gnu.org/software/coreutils/)

## Repo klonen
So kannst du das Repo klonen:
```bash
git clone git@gitlab.gwdg.de:asta/dsb/dsb-ansible.git
cd dsb-ansible
```

## Ansible testen
Teste ob alles funktioniert:
```bash
ansible all -m ping -i inventory/ 
ansible all -m ping -i inventory/ --become
```

## [AStA Server Setup](https://gitlab.gwdg.de/asta/dnd-referat/ansible-role-asta_server_setup) auf allen Servern ausführen

```bash
./run-server-setup.sh
```

## Alle Server neustarten

```bash
./run-reboot.sh
```

Note: Dieses Skript installiert auch Updates auf allen Servern.


## FAQ


### Wie nutze ich dieses Ansible Inventory in einem Projekt?

Wir gehen davon aus, dass man alle Projekte in einem Ordner hat (hier: `~/dsb/`):

- `~/dsb/`:
	- `dsb-ansible/`: Dieses Repo.
	- `your-project/`: Das Projekt welches dieses Ansible Inventory nutzen möchte.

Das Projekt `your-project/` kann nun auf dieses Inventory über den folgenden Pfad zugreifen: `../dsb-ansible/inventory/`

Wichtig ist, dass `your-project/` in der README darauf hinweist, dass es das DSB Ansible Projekt in dem Ordner über sich erwartet.
(Am besten mit einem Link auf diese Frage in diesem FAQ.)


## URLs für [AStA SSH CA](https://gitlab.gwdg.de/asta/dnd-referat/ssh-ca) und [AStA SSH Config](https://gitlab.gwdg.de/asta/dnd-referat/asta-ssh-config)

Inventory als CSV: https://asta.pages.gwdg.de/dsb/dsb-ansible/ssh_ca_servers.csv

Inventory als JSON: https://asta.pages.gwdg.de/dsb/dsb-ansible/ssh_ca_servers.json

## Serverliste/Dokumentation

Aus dem Ansible Inventory (bzw. aus der [AStA SSH CA Inventory JSON](https://asta.pages.gwdg.de/dsb/dsb-ansible/ssh_ca_servers.json)) generieren wir auch die Serverdokumentation.

Diese findest du hier: https://asta.pages.gwdg.de/dsb/dsb-ansible/

## Technisch interessante Befehle
Inventory als JSON exportieren:
```bash
ansible-inventory -i inventory/ --list --export
```
Alle "Server Fakten" anzeigen:
```bash
ansible all -m ansible.builtin.setup -i inventory/
```
Befehl auf allen Servern ausführen:
```bash
ansible all -m shell -i inventory/ -a "echo Hello World"
```

## Weiteres Lesematerial (Ansible Dokumentation)
- https://docs.ansible.com/ansible/latest/index.html
- https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html
- https://docs.ansible.com/ansible/latest/user_guide/sample_setup.html
