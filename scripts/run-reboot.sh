#!/bin/bash

# Exit on error
set -e

function print_error_and_exit() {
	echo -e "\n\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[31m[ERROR] ${1}\e[0m\n" >&2
	exit 1
}

function print_task() {
	echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[33m[TASK] \e[0m${1}\e[0m" >&2
}

function print_info() {
	echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[36m[INFO] \e[0m${1}\e[0m" >&2
}

function print_question() {
	echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[34m[QUESTION] \e[0m${1}\e[0m" >&2
}

function ask_yon() {
	local yon=""
	while true ; do
		printf '\r'
		read -p "[Y/N]? " -N 1 -r yon
		if [ "x${yon}" = "xy" ] || [ "x${yon}" = "xY" ]; then
			printf '\n'
			true; return
		elif [ "x${yon}" = "xn" ] || [ "x${yon}" = "xN" ]; then
			printf '\n'
			false; return
		fi
	done
}

################################## MAIN #######################################

# Argumente parsen
if [ "$#" -ge "1" ] && [ "x$1" = "xyes" ]; then
	AUTO_YES="yes"
else
	AUTO_YES="no"
fi


# Gehe in den dsb-ansible/ Ordner
if [ -L "$0" ]; then
	# Script ist ein symlink -> Script befindet sich im dsb-ansible/ Ordner
	pushd "$(dirname "$0")" >/dev/null
else
	# Script ist kein symlink -> originales Script vom scripts/ Ordner wird ausgeführt
	pushd "$(dirname "$0")/.." >/dev/null
fi

# Das Inventory aktualisieren.
print_task "Pull DSB-Ansible Repo"
git pull --recurse-submodules

## Die SSH Config aktualisieren.
#print_question "Do you want me to update your local SSH config (~/.ssh/asta-ssh-config/)?"
#if ask_yon ;  then
#	print_task "Update SSH Config"
#	~/.ssh/asta-ssh-config/scripts/update.sh
#fi

## Stelle sicher, das der Nutzer authentifiziert ist (siehe https://gitlab.gwdg.de/asta/dnd-referat/asta-ssh-config).
## Note: Überprüft nur Authentifizierung NICHT Authorisierung.
#print_task "Authenticating"
#~/.ssh/asta-ssh-config/scripts/auth.sh

if [ "${AUTO_YES}" != "yes" ]; then
	print_task "Getting current uptime"
	ansible all -m shell -i inventory/ -a "uptime" || true
	print_question "Are you sure you want to reboot all servers?"
	print_info "You can omit this question by running this script as follows: $0 yes"
	if ! ask_yon ; then
		print_error_and_exit "User doesn't want to reboot all servers."
	fi
fi

# Alle Server neustarten
print_task "Running reboot Playbook"
ansible-playbook -i inventory/ reboot.yml

# Diese Zeile wird selten erreicht werden, da der ansible-playbook Befehl sagt, dass es einen Fehler gab, wenn Server unreachable sind.
print_info "Done without errors."

popd >/dev/null
