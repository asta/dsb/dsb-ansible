#!/bin/bash

######################### FUNCTIONS ################################

is_host_ssh_ca_managed() {
	if [ "x$(echo "${INVJSON}" | jq -j "._meta.hostvars[\"${1}\"].ssh_ca_managed")" = "xtrue" ] ; then
		true; return
	else
		false; return
	fi
}

get_host_public_ip() {
	echo "${INVJSON}" | jq -j "._meta.hostvars[\"${1}\"].public_ip"
}

get_host_public_domains() {
	echo "${INVJSON}" | jq -j "._meta.hostvars[\"${1}\"].public_domains"  | grep '\"' | sed 's,^ *"\([^"]*\)".*$,\1,g'
}



get_host_asta_meta() {
	echo "${INVJSON}" | jq -j "._meta.hostvars[\"${1}\"].asta_meta"
}






get_children_groups_of_group() {
	echo "${INVJSON}" | jq -j ".[\"${1}\"].children"  | grep '\"' | sed 's,^ *"\([^"]*\)".*$,\1,g'
}

is_group_child_of_group() {
	local tmpcurgroup=""
	get_children_groups_of_group "${2}" | while read tmpcurgroup ; do
		if [ "x${tmpcurgroup}" = "x${1}" ]; then
			return 1; # returns from the subshell created by the pipeline
		fi
	done
	if [ "x${?}" = "x1" ]; then
		true; return
	else
		false; return
	fi
	
}

# may contain duplicates
get_groups_containing_group() {
	local groupname=""
	echo "${GROUPNAMES}" | while read groupname ; do
		if is_group_child_of_group "${1}" "${groupname}" ; then
			#echo "group \"${1}\" is child of group \"${groupname}\"" 1>&2
			echo "${groupname}"
			get_groups_containing_group "${groupname}"
		fi
	done
}








get_hosts_in_group() {
	echo "${INVJSON}" | jq -j ".[\"${1}\"].hosts"  | grep '\"' | sed 's,^ *"\([^"]*\)".*$,\1,g'
}

is_host_in_group() {
	local tmpcurhost=""
	get_hosts_in_group "${2}" | while read tmpcurhost ; do
		if [ "x${tmpcurhost}" = "x${1}" ]; then
			return 1; # returns from the subshell created by the pipeline
		fi
	done
	if [ "x${?}" = "x1" ]; then
		true; return
	else
		false; return
	fi
	
}

# may contain duplicates
get_groups_containing_host_with_duplicates() {
	local groupname=""
	echo "${GROUPNAMES}" | while read groupname ; do
		if is_host_in_group "${1}" "${groupname}" ; then
			#echo "host \"${1}\" is in group \"${groupname}\"" 1>&2
			echo "${groupname}"
			get_groups_containing_group "${groupname}"
		fi
	done
}

get_groups_containing_host() {
	get_groups_containing_host_with_duplicates "${1}" | uniq -u
}




printusage() {
	echo "Usage: ${1} <output_type (json|csv)> <inventory location>"
}




######################### MAIN ################################

# Fail on error
set -e

if [ "x$#" != "x2" ]; then
	echo "Not enough or too many arguments!"
	printusage "${0}"
	exit 1
fi



OUTPUT_TYPE="${1}"
INVENTORY_LOCATION="${2}"

INVJSON="$(ansible-inventory -i "${INVENTORY_LOCATION}" --list --export | jq -c '.')"
HOSTNAMES="$(echo "${INVJSON}" | jq -j '._meta.hostvars|keys'  | grep '\"' | sed 's,^ *"\([^"]*\)".*$,\1,g')"
HOSTNAMES_AS_CSL="$(echo "${HOSTNAMES}" | tr '\n' ',' | sed 's/,$//')"
GROUPNAMES="$(echo "${INVJSON}" | jq -j '.|keys'  | grep -v '\"_meta\"'  | grep '\"' | sed 's,^ *"\([^"]*\)".*$,\1,g')"



if [ "x${OUTPUT_TYPE}" = "xjson" ]; then # JSON

	#{
	#	"servers": {
	#		"asta-test01": {
	#			"ssh_ca_managed": true,
	#			"public_ip":"141.5.101.41",
	#			"public_domains": [
	#				"c101-041.cloud.gwdg.de"
	#			],
	#			"custom_principals": [
	#				"all",
	#				"test_servers"
	#			]
	#		}
	#
	#	},
	#	"servernames": [
	#		"asta-test01"
	#	]
	#}

	printf "{\n"

	printf "\"servers\": {\n"
	ISFIRST="true"
	echo "${HOSTNAMES}" | while read curhost ; do
		curhost_public_ip="$(get_host_public_ip "${curhost}")"
		curhost_public_domains="$(get_host_public_domains "${curhost}")"
		curhost_public_domains_as_csl="$(echo "${curhost_public_domains}" | tr '\n' ',' | sed 's/,$//')"
		curhost_groups="$(get_groups_containing_host "${curhost}")"
		curhost_groups_as_csl="$(echo "${curhost_groups}" | tr '\n' ',' | sed 's/,$//')"
		curhost_asta_meta="$(get_host_asta_meta "${curhost}")"

		if [ "x${ISFIRST}" = "xtrue" ]; then
			ISFIRST="false"
		else
			printf ","
		fi

		printf "\"%s\": {\n" "${curhost}"

		printf "\"ssh_ca_managed\": "
		if is_host_ssh_ca_managed "${curhost}" ; then
			printf "true"
		else
			printf "false"
		fi
		printf ",\n"

		printf "\"public_ip\": \"%s\",\n" "${curhost_public_ip}"

		printf "\"public_domains\": [\n"
		echo "${curhost_public_domains_as_csl}" | sed 's/,/",\n"/g;s/^/"/;s/$/"/'
		printf "],\n"

		printf "\"asta_meta\": "
		echo "${curhost_asta_meta}"
		printf ",\n"

		printf "\"custom_principals\": [\n"
		echo "${curhost_groups_as_csl}" | sed 's/,/",\n"/g;s/^/"/;s/$/"/'
		printf "]\n"

		printf "}\n"
	done
	printf "},\n"


	printf "\"servernames\": [\n"
	echo "${HOSTNAMES_AS_CSL}" | sed 's/,/",\n"/g;s/^/"/;s/$/"/'
	printf "]\n"


	printf "}\n"

elif [ "x${OUTPUT_TYPE}" = "xcsv" ]; then # CSV
	printf "ssh_ca_managed|name|public_ip|public_domains|custom_principals\n"
	
	echo "${HOSTNAMES}" | while read curhost ; do
		curhost_public_ip="$(get_host_public_ip "${curhost}")"
		curhost_public_domains="$(get_host_public_domains "${curhost}")"
		curhost_public_domains_as_csl="$(echo "${curhost_public_domains}" | tr '\n' ',' | sed 's/,$//')"
		curhost_groups="$(get_groups_containing_host "${curhost}")"
		curhost_groups_as_csl="$(echo "${curhost_groups}" | tr '\n' ',' | sed 's/,$//')"
		if is_host_ssh_ca_managed "${curhost}" ; then
			printf "t"
		else
			printf "f"
		fi
		printf "|%s|%s|%s|%s\n" "${curhost}" "${curhost_public_ip}" "${curhost_public_domains_as_csl}" "${curhost_groups_as_csl}"
	
	done
else # UNKNOWN
	echo "Invalid output_type!"
	printusage "${0}"
	exit 1
fi
