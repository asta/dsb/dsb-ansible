#!/bin/bash

# Exit on error
set -e


function print_task() {
	echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[33m[TASK] \e[0m${1}\e[0m" >&2
}

function print_info() {
	echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[36m[INFO] \e[0m${1}\e[0m" >&2
}

function print_question() {
	echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[35m=> \e[34m[QUESTION] \e[0m${1}\e[0m" >&2
}

function ask_yon() {
	local yon=""
	while true ; do
		printf '\r'
		read -p "[Y/N]? " -N 1 -r yon
		if [ "x${yon}" = "xy" ] || [ "x${yon}" = "xY" ]; then
			printf '\n'
			true; return
		elif [ "x${yon}" = "xn" ] || [ "x${yon}" = "xN" ]; then
			printf '\n'
			false; return
		fi
	done
}


################################## MAIN #######################################


# Gehe in den dsb-ansible/ Ordner
if [ -L "$0" ]; then
	# Script ist ein symlink -> Script befindet sich im dsb-ansible/ Ordner
	pushd "$(dirname "$0")" >/dev/null
else
	# Script ist kein symlink -> originales Script vom scripts/ Ordner wird ausgeführt
	pushd "$(dirname "$0")/.." >/dev/null
fi

# Das Inventory aktualisieren.
print_task "Pull DSB-Ansible Repo"
git pull --recurse-submodules

## Die SSH Config aktualisieren.
#print_question "Do you want me to update your local SSH config (~/.ssh/asta-ssh-config/)?"
#if ask_yon ;  then
#	print_task "Update SSH Config"
#	~/.ssh/asta-ssh-config/scripts/update.sh
#fi


## Stelle sicher, das der Nutzer authentifiziert ist (siehe https://gitlab.gwdg.de/asta/dnd-referat/asta-ssh-config).
## Note: Überprüft nur Authentifizierung NICHT Authorisierung.
#print_task "Authenticating"
#~/.ssh/asta-ssh-config/scripts/auth.sh

# Aktualisiere die Ansible Abhängigkeiten
print_task "Updating requirements"
# Das --force ist wichtig, damit das ansible-role-asta_server_setup jedes mal neu gepulled wird!
ansible-galaxy role install --force -r requirements.yml 

# Server-Setup ausführen
print_task "Running server-setup Playbook"
#print_info "Das Vault Passwort findest du in der 'AStA Admin'-Organisation im 'vars/vault.yml'-Feld unter 'Ansible Vault' im Bitwarden."
#ansible-playbook -i inventory/ server-setup.yml --ask-vault-pass
ansible-playbook -i inventory/ server-setup.yml

# Diese Zeile wird selten erreicht werden, da der ansible-playbook Befehl sagt, dass es einen Fehler gab, wenn Server unreachable sind.
print_info "Done without errors."

popd >/dev/null
